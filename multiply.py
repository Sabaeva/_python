print('Введите два числа, чтобы получить их сумму')
print('Введите q для выхода из программы. ')

while True:
    first = input('\nПервое число: ')
    if first == 'q':
        break
    second = input('\nВторое число: ')
    if second == 'q':
        break
    try:
        print(int(first) + int(second))
    except ValueError:
        print('Введите числа!')
