""" Использование yield/yield from """


def concat_seq_v1 (s1, s2):
    for element in s1:
        yield element
    for element in s2:
        yield element


def concat_seq_v2 (s1, s2):
    yield from s1
    yield from s2


seq1 = range(10)
seq2 = range(10, 20)
result1 = concat_seq_v1(seq1, seq2)

for i in result1:
    print(i)

result2 = concat_seq_v2(seq1, seq2)

for i in result2:
    print(i)
