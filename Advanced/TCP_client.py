""" Пример использования TCP. TCP client socket """
import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(('127.0.0.1', 8888))
sock.send(b'Test Message')
result = sock.recv(64)
print('Response: ', result)
sock.close()
