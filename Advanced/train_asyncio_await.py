""" Использование asyncio """
import asyncio


async def async_worker (number, divider):
    print('Worker {} started'.format(number))
    await asyncio.sleep(1)
    print(number / divider)
    return number / divider


async def gather_worker ():
    result = await asyncio.gather(
        async_worker(50, 1),
        async_worker(50, 2),
        async_worker(50, 3),
        async_worker(50, 4),
        async_worker(50, 5),
        async_worker(50, 6),
        async_worker(50, 7),
        async_worker(50, 8),
        async_worker(50, 9),
        async_worker(50, 10),
    )
    print(result)


event_loop = asyncio.get_event_loop()
task_list = [
    async_worker(30,10),
    asyncio.ensure_future(async_worker(30,10)),
    event_loop.create_task(gather_worker()),
]
tasks = asyncio.wait(task_list)
event_loop.run_until_complete(tasks)
event_loop.close()
