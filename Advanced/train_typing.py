from typing import List


class User:
    def __init__ (self, first_name: str, last_name: str):
        self.first_name = first_name
        self.last_name = last_name


def create_users_ver1 (first_names: list, last_names: list) -> list:
    users = []
    items = zip(first_names, last_names)
    for first_name, last_name in items:
        users.append(User(first_name, last_name))
    return users


def create_users_ver2 (first_names: List[str],
                       last_names: List[str]) -> List[User]:
    users = []
    items = zip(first_names, last_names)
    for first_name, last_name in items:
        users.append(User(first_name, last_name))
    return users


users1 = create_users_ver2(['a1', 'a2'], ['b1', 'b2'])
users2 = create_users_ver1(['a1', 10], ['b1', 'b2'])
users3 = create_users_ver2(['a1', 'a2'], ['b1', []]) #при запуске с
# помощью mypy вылетает ошибка, при обычном запуске - все ок
