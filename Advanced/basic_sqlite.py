import sqlite3

conn = sqlite3.connect('db.sqlite3')

conn.execute('CREATE TABLE "users"(id, first_name, last_name, '
             'birthday)')
conn.execute('SELECT * FROM "users"')
conn.execute("""
  INSERT INTO users(id, first_name, last_name, birthday) 
  VALUES (1, "Eugene", "Petrov", "11-01-1986")
  """)

conn.execute("""
  INSERT INTO users(id, first_name, last_name, birthday) 
  VALUES (2, "Masha", "Ivanova", "31-04-1978"),
         (3, "Anna", "Sidorova", "14-12-1983")
  """)
     
users = conn.execute('SELECT * FROM "users"').fetchall()
print(users)
cursor = conn.execute('SELECT * FROM "users";')
print(cursor.fetchone())
print(cursor.fetchone())
print(cursor.fetchone())
# cursor.close()

try:
    cursor = conn.execute('SELECT * FROM "tags"')
except sqlite3.OperationalError as e:
    print(e)


# first_name = '"Masha"'
# sql_text = 'SELECT * FROM users WHERE first_name = "%s"' % (first_name)
# print(sql_text)

cursor.execute('SELECT * FROM "users" WHERE id = ?', (10,))
cursor.execute('SELECT * FROM "users" WHERE id = :id', {'id':20})
conn.close()
