from typing import TypeVar, List, Sequence, Iterable

IntOrStr = TypeVar('IntOrStr', int, str, float)


def copy_list (sequence: Iterable[IntOrStr]) -> List[IntOrStr]:
    new_list: List[IntOrStr] = []
    for element in sequence:
        new_list.append(element)
    return new_list


value1 = copy_list([1, 2, 3])
value2 = copy_list([1.2, 3.4, 5.6])
value3 = copy_list(['3434', 'fdfs', 'fdee'])