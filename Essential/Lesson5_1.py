def print_matrix (matrix):
    for row in matrix:
        print('_'.join(str(x) for x in row))


matrix = [[0] * 5] * 5
"""Каждая строка - копия предыдущей, с одной и той же ссылкой на 
объекты"""
matrix[1][3] = 6
print_matrix(matrix)

matrix = [[0] * 5 for _ in range(5)]
"""У каждой строки свои ссылки на объекты"""
matrix[1][3] = 6
print()
print_matrix(matrix)
