def fibonacci (count):
    """ Вывод чисел Фибоначчи"""
    first, second = 0, 1
    for _ in range(count):
        yield second
        first, second = second, first + second

count = int(input('How many Fibonacci numbers to print?'))

for i in fibonacci(count):
    print(i)

