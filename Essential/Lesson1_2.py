class Rectangle:
    def __init__ (self, a, b):
        self.a = a
        self.b = b

    def __repr__ (self):
        return "Rectangle(%.1f, %.1f)" % (self.a, self.b)


class Circle:
    def __init__ (self, rad):
        self.rad = rad

    def __repr__ (self):
        return "Circle (%.1f)" % self.rad

    @classmethod
    def from_rectangle (cls, rectangle):
        rad = (rectangle.a ** 2 + rectangle.b ** 2) ** 0.5 / 2
        return cls(rad)


def main ():
    rectangle = Rectangle(3, 4)
    print(rectangle)

    first_circle = Circle(1)
    print(first_circle)

    second_circle = Circle.from_rectangle(rectangle)
    print(second_circle)


if __name__ == "__main__":
    main()
