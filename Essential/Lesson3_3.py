def main():
    try:
        raise ValueError('Value is incorrect')
    except ValueError as error:
        print('Exception:', error)
        raise


try:
    main()
except ValueError:
    print("ValueError detected")