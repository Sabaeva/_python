def main ():
    """ Обработка исключений """
    while True:
        try:
            first = float(input('First_number: '))
            second = float(input('Second_number: '))
            print('Result: ', first / second)
            break
        except(ValueError, ZeroDivisionError) as error:
            print('Error: ', error)
            print('Please try again')


if __name__ == "__main__":
    main()
