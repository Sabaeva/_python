import unittest
from city_country import formatted_city_country


class CityCountryTest(unittest.TestCase):
    """Тест для city_country"""

    def test_city_country (self):
        formatted_name = formatted_city_country('Минск', 'беларусь')
        self.assertEqual(formatted_name, 'Минск, Беларусь')

    def test_city_country_population (self):
        formatted_name_population = formatted_city_country('Минск',
                                                           'беларусь',
                                                           200)
        self.assertEqual(formatted_name_population, 'Минск, '
                                                    'Беларусь - '
                                                    'Население 200')


if __name__ == '__main__':
    unittest.main()
