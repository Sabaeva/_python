
def count_the(filename):
    """Подсчет вхождений слова 'the' в текст"""
    try:
        with open(filename) as file:
            contents = file.read()
    except FileNotFoundError:
        print(f'Файла с именем {filename} не существует')
    else:
        # word_the = contents.lower().count('the ')
        print(contents.lower().count('the '))

filename = 'book.txt'
count_the(filename)