from get_formatted_name import get_formatted_name

print('Введите "q" для выхода')
while True:
    first = input('\nВведите ваше имя: ')
    if first == 'q':
        break
    last = input('Введите вашу фамилию: ')
    if last == 'q':
        break

    formatted_name = get_formatted_name(first,last)
    print(f'\tОтформатированное имя: {formatted_name}')