def read_name(filename):
    try:
        with open(filename, encoding='utf-8') as file:
            names = file.read()
    except FileNotFoundError:
        print(f'Файл с именем {filename} не существует!')
    else:
        print(names)


filenames = ['dogs.txt', 'cats.txt']
for name in filenames:
    read_name(name)
