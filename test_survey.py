import unittest
from survey import AnonSurvey


class TestAnonSurvey(unittest.TestCase):
    """Тесты для класса AnonSurvey"""

    def setUp (self):
        """Создание опроса и набора ответов для всех тестовых методов"""
        question = "What language did you first learn to speak?"
        self.my_survey = AnonSurvey(question)
        self.responses = ['Russian', 'English', 'Tatar']

    def test_store_single_response (self):
        """Проверка - один ответ сохранен правильно?"""
        self.my_survey.store_response(self.responses[0])
        self.assertIn(self.responses[0], self.my_survey.responses)

    def test_store_three_response (self):
        """Проверка - три ответа сохранены правильно?"""
        for response in self.responses:
            self.my_survey.store_response(response)
        for response in self.responses:
            self.assertIn(response, self.my_survey.responses)


if __name__ == 'main':
    unittest.main()
