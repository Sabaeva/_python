import unittest
from Employee import Employee


class TestEmployee(unittest.TestCase):
    """Тесты для Empoyee."""

    def setUp(self):
        """Создание эксзепляра Employee для всех тестовых методов."""
        self.my_employee = Employee('Tanya', 'Sabaeva', 1000)

    def test_default_raise (self):
        """Правильно ли выдается зп по умолчанию"""
        self.my_employee.give_raise()
        self.assertEqual(self.my_employee.salary, 6000)


    def test_custom_raise (self):
        """Правильно ли выдается заданная зп"""
        self.my_employee.give_raise(1000)
        self.assertEqual(self.my_employee.salary, 2000)

