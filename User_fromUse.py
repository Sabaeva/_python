from Use import User


class Admin(User):

    def __init__(self, first_name, last_name):
        super().__init__(first_name, last_name)


class Priveleges():

    def __init__(self):
        self.priveleges = (
        'разрешено удалять сообщения',
        'разрешено удалять пользователей',
        'разрешено банить пользователей'
        )

    def show_priveleges(self):
        for privelege in self.priveleges:
            print(privelege)
