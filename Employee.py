class Employee():
    """Представляет работника"""

    def __init__(self, first_name, last_name, salary):
        self.first_name = first_name
        self.last_name = last_name
        self.salary = salary

    def give_raise(self, custom_raise = ''):
        if custom_raise:
            self.salary += custom_raise
        else:
            self.salary += 5000

