import unittest
from get_formatted_name import get_formatted_name


class NamesTestCase(unittest.TestCase):
    """Тесты для 'get_formatted_name.py'"""

    def test_first_last_names(self):
        """Работает ли со вторым именем?"""
        formatted_name = get_formatted_name('janis', 'joplin')
        self.assertEqual(formatted_name, 'Janis Joplin')

    def test_first_last_middle_names(self):
        """Работает ли со вторым именем?"""
        formatted_name = get_formatted_name('janis', 'joplin', 'jane')
        self.assertEqual(formatted_name, 'Janis Jane Joplin')


if __name__ == '__main__':
    unittest.main()
