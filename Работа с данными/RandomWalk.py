from random import choice

class RandomWalk():
    """Генерирование случайных блужданий"""

    def __init__ (self, num_points=5000):
        """Инициализирует атрибуты блуждания."""
        self.num_points = num_points
        # начальная позиция - (0,0)
        self.x = [0]
        self.y = [0]

    def get_step(self):
        """Вычисляет направление и длину шага"""

        direction = choice([1, -1])
        distance = choice([0, 1, 2, 3, 4])
        step = direction * distance
        return step

    def fill_walk (self):
        """Вычисляет все точки блуждания"""

        # Шаги продолжают генерироваться до достижения нужной длины
        while len(self.x) < self.num_points:
            x_step = self.get_step()
            y_step = self.get_step()

            # Отклонение нулевых перемещений
            if x_step == 0 and y_step == 0:
                continue

            # Вычисление следующих значений х и у
            x_new = self.x[-1] + x_step
            y_new = self.y[-1] + y_step

            self.x.append(x_new)
            self.y.append(y_new)
