from plotly.graph_objs import Bar, Layout
from plotly import offline

from die import Die

# Создание кубиков
die_1 = Die()
die_2 = Die()
die_3 = Die()

# Моделирование серии бросков
results = []
[results.append(die_1.roll() * die_2.roll() * die_3.roll()) for
                roll_num in range(1000)]

# Анализ результатов
frequencies = []
max_result = die_1.num_sides * die_2.num_sides * die_3.num_sides
[frequencies.append(results.count(value)) for value in range(1,
                                                             max_result+1)]
# Визуализация результатов
x = list(range(1, max_result + 1))
data = [Bar(x=x, y=frequencies)]
x_axis_config = {'title': 'Result'}
y_axis_config = {'title': 'Frequency of Result'}
my_layout = Layout(title='Result of multiplying of rolling three D6 '
                         'dice 1000 times',
                   xaxis=x_axis_config, yaxis=y_axis_config)
offline.plot({'data': data, 'layout': my_layout},
             filename='d6_multiplying.html')
