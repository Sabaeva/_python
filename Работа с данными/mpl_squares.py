import matplotlib.pyplot as plt

input_values = list(range(1, 5001))
squares = [value ** 2 for value in input_values]
plt.style.use('seaborn')
fig, ax = plt.subplots()
ax.scatter(input_values, squares, c = squares, cmap = plt.cm.Reds,
           s=10)

# ax.plot(input_values, squares, linewidth=3)
ax.set_title("Square Numbers", fontsize=24)
ax.set_xlabel("Value", fontsize=14)
ax.set_ylabel("Square of Value", fontsize=14)
ax.tick_params(axis="both", labelsize=14)
# ax.axis([0, 1100, 0, 1100000])
plt.show()
