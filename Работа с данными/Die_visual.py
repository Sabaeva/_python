from plotly.graph_objs import Bar, Layout
from plotly import offline

from die import Die

# Создание кубиков
die_1 = Die()
die_2 = Die()
die_3 = Die()

# Моделирование серии бросков
results = []
for roll_num in range(1000):
    result = die_1.roll() + die_2.roll() + die_3.roll()
    results.append(result)


# Анализ результатов
frequencies = []
max_result = die_1.num_sides + die_2.num_sides + die_3.num_sides
for value in range(3, max_result + 1):
    frequency = results.count(value)
    frequencies.append(frequency)

# Визуализация результатов
x = list(range(3, max_result + 1))
data = [Bar(x=x, y=frequencies)]
x_axis_config = {'title': 'Result', 'dtick': 1}
y_axis_config = {'title': 'Frequency of Result'}
my_layout = Layout(title='Result of rolling three D6 dice 1000 times',
                   xaxis=x_axis_config, yaxis=y_axis_config)
offline.plot({'data': data, 'layout': my_layout}, filename='d8.html')
